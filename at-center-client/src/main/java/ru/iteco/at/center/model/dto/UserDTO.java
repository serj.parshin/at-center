package ru.iteco.at.center.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.Role;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(of = "id", callSuper = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String token;

    @Nullable
    private Role role = Role.USER;

    @Nullable
    private String email;
}
