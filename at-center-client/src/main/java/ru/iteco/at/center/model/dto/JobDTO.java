package ru.iteco.at.center.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(of = "id", callSuper = true)
public class JobDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String userToken;

    @Nullable
    private String name;

    @Nullable
    private String buildNumber;

    @Nullable
    private JobInstanceDTO jobInstanceDTO;
}
