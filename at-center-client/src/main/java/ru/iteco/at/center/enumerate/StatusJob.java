package ru.iteco.at.center.enumerate;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public enum StatusJob implements Serializable {

    PLANNED("Planned"),
    INPROCESS("In process"),
    DONE("done");

    @NotNull
    public final String label;

    public String getLabel() {
        return label;
    }

    StatusJob(String label) {
        this.label = label;
    }
}
