package ru.iteco.at.center.model.dto;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.SystemCapacity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class SystemDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String name;

    @Nullable
    private SystemCapacity  systemCapacity;;

    @NotNull
    private List<JobInstanceDTO> jobInstanceDTOList = new ArrayList<>();
}
