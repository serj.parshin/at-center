package ru.iteco.at.center.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.StatusJob;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = "id", callSuper = true)
public class JobInstanceDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String jobId;

    @Nullable
    private String userId;

    @Nullable
    private SystemDTO systemDTO;

    @Nullable
    private StatusJob statusJob;

    @NotNull
    private List<MetrikDTO> metrikDTOList = new ArrayList<>();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Nullable
    private String memoryOfJob;
}
