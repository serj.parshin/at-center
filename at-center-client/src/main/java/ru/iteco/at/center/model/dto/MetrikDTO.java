package ru.iteco.at.center.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.TypeMetrik;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MetrikDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String jobInstanceId;

    @Nullable
    private String name;

    @Nullable
    private TypeMetrik type;

    @NotNull
    private List<MetrikValueDTO> metrikValueDTOList = new ArrayList<>();
}
