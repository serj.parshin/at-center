package ru.iteco.at.center.enumerate;

import org.jetbrains.annotations.NotNull;

public enum Role {

    GUEST("Guest"),
    USER("User"),
    ADMIN("Administrator");

    @NotNull
    public final String label;

    public String getLabel() {
        return label;
    }

    Role(String label) {
        this.label = label;
    }
}
