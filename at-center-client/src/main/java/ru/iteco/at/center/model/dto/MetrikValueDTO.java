package ru.iteco.at.center.model.dto;

import lombok.*;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class MetrikValueDTO extends AbstractEntityDTO implements Serializable {

    @Nullable
    private String metrikId;

    @Nullable
    private String value;

    @Nullable
    private Date timeStamp;
}
