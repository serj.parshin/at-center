package ru.iteco.at.center.enumerate;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public enum TypeMetrik implements Serializable {

    CURRENT_AMOUNT("Current amount"),
    TIME_INTERVALS("Time interval"),
    LOAD_AVERAGE("Load average");

    @NotNull
    public final String label;

    public String getLabel() {
        return label;
    }

    TypeMetrik(String label) {
        this.label = label;
    }
}
