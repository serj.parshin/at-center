package ru.iteco.at.center.enumerate;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public enum SystemCapacity implements Serializable {

    CAPACITY_32("x32"),
    CAPACITY_64("x64");

    @NotNull
    public final String label;

    public String getLabel() {
        return label;
    }

    SystemCapacity(String label) {
        this.label = label;
    }
}
