package ru.iteco.at.center.api.client;

import org.junit.jupiter.api.Test;
import ru.iteco.at.center.model.dto.JobDTO;
import ru.iteco.at.center.model.dto.JobInstanceDTO;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class JobFeignClientTest {

    private static final Integer PORT = 8080;

    @Test
    void post() {
        final JobInstanceDTO jobInstanceDTO = new JobInstanceDTO();
        final JobDTO jobDTO = new JobDTO();
        jobInstanceDTO.setJobId(jobDTO.getId());
        jobDTO.setUserToken("$2a$10$L3fcHBxxDO4KRJHFd2lvO.J0aynxxt63S5OOQtpOs7aGvQ8EHBELS");
        jobDTO.setName("job from client");
        jobDTO.setBuildNumber("1.0.0");
        jobDTO.setJobInstanceDTO(jobInstanceDTO);

        final JobFeignClient client = JobFeignClient.client("http://localhost:" + PORT + "/api/job");
        assertNotNull(client);
        client.post(jobDTO);
    }
}