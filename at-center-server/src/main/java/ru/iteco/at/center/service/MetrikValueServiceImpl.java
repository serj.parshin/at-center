package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.MetrikValueRepository;
import ru.iteco.at.center.api.service.MetrikValueService;
import ru.iteco.at.center.exception.MetrikNotFoundException;
import ru.iteco.at.center.model.entity.MetrikValue;

@Service
public class MetrikValueServiceImpl implements MetrikValueService {

    @Autowired
    private MetrikValueRepository metrikValueRepository;

    @Transactional
    public MetrikValue save(MetrikValue metrikValue) {
        return metrikValueRepository.save(metrikValue);
    }

    public MetrikValue findById(String id) {
        return metrikValueRepository.findById(id).orElseThrow(MetrikNotFoundException::new);
    }

    public boolean existsById(String id) {
        return metrikValueRepository.existsById(id);
    }

    @Transactional
    public void deleteById(String id) {
        metrikValueRepository.deleteById(id);
    }

    @Transactional
    public void delete(MetrikValue metrikValue) {
        metrikValueRepository.delete(metrikValue);
    }

    @Nullable
    public Iterable<MetrikValue> findAllByMetrik_id(@NotNull String metrikId) {
        return metrikValueRepository.findAllByMetrik_id(metrikId);
    }

    public boolean existsByMetrik_id(@NotNull String metrikId) {
        return metrikValueRepository.existsByMetrik_id(metrikId);
    }

    @Transactional
    public void deleteAllByMetrik_id(@NotNull String metrikId) {
        metrikValueRepository.deleteAllByMetrik_id(metrikId);
    }
}
