package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.UserRepository;
import ru.iteco.at.center.api.service.AuthenticationFacade;
import ru.iteco.at.center.api.service.TokenService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.exception.UserNotFoundException;
import ru.iteco.at.center.model.entity.User;

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenService tokenService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByLogin(username);
    }

    @NotNull
    public User findLoggedUser() {
        final Authentication authentication = authenticationFacade.getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) throw new UserNotFoundException();
        final String currentUserName = authentication.getName();
        return userRepository.findByLogin(currentUserName);
    }

    @NotNull
    public User findById(String id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User create(User user) {
        return userRepository.save(user);
    }

    @Override
    public User generateToken(User user) {
        final String token = tokenService.generateToken();
        user.setToken(token);
        return userRepository.save(user);
    }

    @Override
    public User findByToken(@NotNull final String token) {
        return userRepository.findOptionalByToken(token).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public String findIdByToken(String token) {
        return userRepository.findIdByToken(token);
    }
}
