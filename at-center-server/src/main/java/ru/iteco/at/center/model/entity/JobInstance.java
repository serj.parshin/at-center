package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.iteco.at.center.enumerate.StatusJob;
import ru.iteco.at.center.model.dto.JobInstanceDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_jobinstance")
@EqualsAndHashCode(of = "id", callSuper = true)
public class JobInstance extends AbstractEntity implements Serializable {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "system_id")
    private OperationSystem operationSystem;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "jobInstance",
            orphanRemoval = true,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Metrik> metrikList = new ArrayList<>();

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dateBegin;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dateEnd;

    @Nullable
    @Enumerated(EnumType.STRING)
    private StatusJob statusJob;

    @Nullable
    private String memoryOfJob;

    @Nullable
    public JobInstanceDTO toDTO() {
        try {
            final JobInstanceDTO dto = new JobInstanceDTO();
            dto.setId(getId());
            dto.setJobId((getJob().getId()));
            dto.setUserId(getUser().getId());
            dto.setSystemDTO(getOperationSystem().toDTO());
            dto.setStatusJob(getStatusJob());
            dto.setMetrikDTOList(metrikList
                    .stream()
                    .map(Metrik::toDTO)
                    .collect(Collectors.toList()));
            dto.setDateBegin(dateBegin);
            dto.setDateEnd(dateEnd);
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
