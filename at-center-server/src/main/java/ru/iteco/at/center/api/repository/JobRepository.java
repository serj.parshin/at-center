package ru.iteco.at.center.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.Job;

@Repository
public interface JobRepository extends JpaRepository<Job, String> {
}
