package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.MetrikValue;

@Repository
public interface MetrikValueRepository extends JpaRepository<MetrikValue, String> {

    @Nullable
    Iterable<MetrikValue> findAllByMetrik_id(@NotNull final String metrikId);

    void deleteAllByMetrik_id(@NotNull final String metrikId);

    boolean existsByMetrik_id(@NotNull final String metrikId);
}
