package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.JobInstance;

import java.util.Optional;

@Repository
public interface JobInstanceRepository extends JpaRepository<JobInstance, String> {

    @Nullable
    Iterable<JobInstance> findAllByJob_id(@NotNull final String jobId);

    @NotNull
    Optional<JobInstance> findByIdAndJob_id(@NotNull final String id, @NotNull final String jobId);

    boolean existsByJob_Id(@NotNull final String jobId);
}
