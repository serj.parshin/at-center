package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.CategoryJobService;
import ru.iteco.at.center.api.service.JobInstanceService;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.Job;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.model.entity.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "jobBean")
public class JobManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobInstanceService jobInstanceService;

    @Autowired
    private CategoryJobService categoryJobService;

    private Job job = new Job();
    private LineChartModel lineModel = new LineChartModel();

    public String createJob() {
        final User user = userService.findLoggedUser();
        job = new Job();
        return "pretty:createJob";
    }

    public String editJob(@NotNull final Job job) {
        this.job = job;
        return "pretty:editJob";
    }

    public String saveJob() {
        jobService.save(job);
        return "pretty:jobList";
    }

    public String viewJob(@NotNull final Job job) {
        this.job = job;
        return "pretty:viewJob";
    }

    public Iterable<Job> list() {
        final User user = userService.findLoggedUser();
        return jobService.findAll();
    }

    public boolean jobInstanceExists() {
        return jobInstanceService.existsByJobId(job.getId());
    }

    public Iterable<JobInstance> jobInstanceList() {
        return jobInstanceService.findAllByJobId(job.getId());
    }

    public Iterable<Category> categoriesByJobId(@NotNull final String jobId) {
        return categoryJobService.findAllCategoriesByJobId(jobId);
    }

    public void deleteJob(@NotNull final Job job) {
        jobService.delete(job);
    }

    public LineChartModel getLineModel() {
        final ChartData data = new ChartData();
        final LineChartDataSet dataSet = new LineChartDataSet();
        final List<Number> times = new ArrayList<>();
        final List<String> labels = new ArrayList<>();
        jobInstanceList().forEach(jobInstance -> {
            final Long time = jobInstance.getDateEnd().getTime() - jobInstance.getDateBegin().getTime();
            times.add(time);
        });
        for (int i = 1; i <= times.size(); i++) {
            labels.add(String.valueOf(i));
        }
        dataSet.setData(times);
        data.setLabels(labels);
        dataSet.setFill(false);
        dataSet.setLabel("Runtime graph between job runs");
        dataSet.setBorderColor("rgb(0, 129, 194)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
        lineModel.setData(data);
        return lineModel;
    }

    public LineChartModel getLineModelOfMemory() {
        lineModel = new LineChartModel();
        ChartData data = new ChartData();

        LineChartDataSet dataSet = new LineChartDataSet();
        List<Number> valuesMemory = new ArrayList<>();
        final List<String> labels = new ArrayList<>();
        jobInstanceList().forEach(jobInstance -> {
            final long availableMemory = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory() + Runtime.getRuntime().freeMemory());
            final long percentUsedMemory = 100*(Runtime.getRuntime().maxMemory() - availableMemory)/Runtime.getRuntime().maxMemory();
            valuesMemory.add(percentUsedMemory);
        });
        for (int i = 1; i <= valuesMemory.size(); i++) {
            labels.add(String.valueOf(i));
        }
        data.setLabels(labels);
        dataSet.setData(valuesMemory);
        dataSet.setFill(false);
        dataSet.setLabel("Memory usage between it's starts");
        dataSet.setBorderColor("rgb(75, 192, 192)");
        dataSet.setLineTension(0.1);
        data.addChartDataSet(dataSet);
        lineModel.setData(data);
        return lineModel;
    }
}
