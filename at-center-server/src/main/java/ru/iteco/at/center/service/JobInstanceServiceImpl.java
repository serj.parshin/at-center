package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.repository.JobInstanceRepository;
import ru.iteco.at.center.api.service.JobInstanceService;
import ru.iteco.at.center.exception.JobInstanceNotFoundException;
import ru.iteco.at.center.model.entity.JobInstance;

@Service
public class JobInstanceServiceImpl implements JobInstanceService {

    @Autowired
    private JobInstanceRepository jobInstanceRepository;

    @Nullable
    public Iterable<JobInstance> findAllByJobId(@NotNull final String jobId) {
        return jobInstanceRepository.findAllByJob_id(jobId);
    }

    @NotNull
    public JobInstance save(@NotNull final JobInstance job) {
        return jobInstanceRepository.save(job);
    }

    @NotNull
    public JobInstance findById(@NotNull final String id) {
        return jobInstanceRepository.findById(id).orElseThrow(JobInstanceNotFoundException::new);
    }

    @NotNull
    public JobInstance findByIdAndUserId(@NotNull final String id, @NotNull final String jobId) {
        return jobInstanceRepository.findByIdAndJob_id(id, jobId).orElseThrow(JobInstanceNotFoundException::new);
    }

    public boolean existsById(@NotNull final String id) {
        return jobInstanceRepository.existsById(id);
    }

    @Override
    public boolean existsByJobId(@NotNull String jobId) {
        return jobInstanceRepository.existsByJob_Id(jobId);
    }

    public void deleteById(@NotNull final String id) {
        jobInstanceRepository.deleteById(id);
    }

    public void delete(@NotNull final JobInstance jobInstance) {
        jobInstanceRepository.delete(jobInstance);
    }
}
