package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "userBean")
public class UserManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private UserService userService;

    @NotNull
    private User user = new User();

    @Nullable
    private User searchedUser;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public String registrationUser() {
        this.user = new User();
        return "pretty:registration";
    }

    public String saveUser() {
        @Nullable final String password = user.getPassword();
        user.setPassword(passwordEncoder.encode(password));
        userService.create(user);
        return "pretty:registration";
    }

    public String viewUser(){
        this.user = userService.findLoggedUser();
        return "pretty:profileUser";
    }

    public void generateToken(){
        this.user = userService.generateToken(user);
    }
}

