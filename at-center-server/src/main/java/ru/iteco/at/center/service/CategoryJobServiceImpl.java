package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.at.center.api.repository.CategoryJobRepository;
import ru.iteco.at.center.api.service.CategoryJobService;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.CategoryJob;
import ru.iteco.at.center.model.entity.Job;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CategoryJobServiceImpl implements CategoryJobService {

    @Autowired
    private CategoryJobRepository categoryJobRepository;

    @Nullable
    public Iterable<Category> findAllCategoriesByJobId(@NotNull final String jobId) {
        return StreamSupport
                .stream(categoryJobRepository.findByJob_id(jobId).spliterator(), false)
                .map(CategoryJob::getCategory)
                .collect(Collectors.toList());
    }

    @Nullable
    public Iterable<Job> findAllJobByCategoryId(@NotNull final String categoryId) {
        return StreamSupport
                .stream(categoryJobRepository.findByCategory_id(categoryId).spliterator(), false)
                .map(CategoryJob::getJob)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsByCategoryId(@NotNull final String id) {
        return categoryJobRepository.existsByCategory_Id(id);
    }
}
