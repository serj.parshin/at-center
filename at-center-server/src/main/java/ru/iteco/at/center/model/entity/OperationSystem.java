package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.enumerate.SystemCapacity;
import ru.iteco.at.center.model.dto.SystemDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_system")
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
public class OperationSystem extends AbstractEntity implements Serializable {

    @Nullable
    private String name;

    @Nullable
    @Enumerated(EnumType.STRING)
    private SystemCapacity  systemCapacity;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "operationSystem")
    transient private List<JobInstance> jobInstanceList;

    public OperationSystem(@Nullable String name, @Nullable SystemCapacity systemCapacity, @Nullable List<JobInstance> jobInstanceList) {
        this.name = name;
        this.systemCapacity = systemCapacity;
        this.jobInstanceList = jobInstanceList;
    }

    @Nullable
    public SystemDTO toDTO() {
        try {
            final SystemDTO dto = new SystemDTO();
            dto.setId(getId());
            dto.setName(getName());
            dto.setSystemCapacity(getSystemCapacity());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
