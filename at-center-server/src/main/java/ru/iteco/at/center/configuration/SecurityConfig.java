package ru.iteco.at.center.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.UserRepository;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.model.entity.User;

import javax.annotation.PostConstruct;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(@NotNull AuthenticationManagerBuilder auth) throws Exception {

        final PasswordEncoder encoder = passwordEncoder();

        auth.userDetailsService(userDetailsService)
                .passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .and().formLogin().loginPage("/login").failureUrl("/login?error=true")
                .and().logout().permitAll().logoutSuccessUrl("/login")
                .and().csrf().disable();

        http.authorizeRequests()
                .antMatchers("/", "/api/**", "/register", "/javax.faces.resource/**", "/resources/**").permitAll()
                .antMatchers("/job**", "/metrik**", "/category**", "/system**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/profile").hasAnyAuthority("ADMIN", "USER");
    }

    @Transactional
    @PostConstruct
    void initDefaultUsers(){
        final User admin = new User();
        admin.setLogin("admin");
        admin.setEmail("admin@admin.admin");
        admin.setRole(Role.ADMIN);
        admin.setPassword(passwordEncoder().encode("admin"));
        if (!userRepository.existsByLogin(admin.getLogin()))
            userRepository.save(admin);

        final User test = new User();
        test.setLogin("test");
        admin.setEmail("test@test.test");
        admin.setRole(Role.USER);
        test.setPassword(passwordEncoder().encode("test"));
        if (!userRepository.existsByLogin(test.getLogin()))
            userRepository.save(test);
    }
}
