package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.CategoryJobService;
import ru.iteco.at.center.api.service.CategoryService;
import ru.iteco.at.center.api.service.JobService;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.Job;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "categoryBean")
public class CategoryManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private JobService jobService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryJobService categoryJobService;

    private Category category = new Category();

    public String createCategory() {
        this.category = new Category();
        return "pretty:createCategory";
    }

    public String editCategory(@NotNull final Category category) {
        this.category = category;
        return "pretty:editCategory";
    }

    public String saveCategory() {
        categoryService.save(category);
        return "pretty:categoryList";
    }

    public String viewCategory(@NotNull final Category category) {
        this.category = category;
        return "pretty:viewCategory";
    }

    public Iterable<Category> categories() {
        return categoryService.findAll();
    }

    public boolean jobsInCategoryExists() {
        return categoryJobService.existsByCategoryId(category.getId());
    }

    public Iterable<Job> jobsOfCategory() {
        return categoryJobService.findAllJobByCategoryId(category.getId());
    }

    public void deleteCategory(@NotNull final Category category) {
        categoryService.delete(category);
    }
}
