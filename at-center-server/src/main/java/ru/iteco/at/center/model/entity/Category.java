package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.model.dto.CategoryDTO;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "app_category")
@EqualsAndHashCode(of = "id", callSuper = true)
public class Category extends AbstractEntity implements Serializable {

    @Nullable
    private String name;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
    private List<CategoryJob> categoryJobList;

    @Nullable
    public CategoryDTO toDTO() {
        try {
            final CategoryDTO dto = new CategoryDTO();
            dto.setId(getId());
            dto.setName(getName());
            dto.setCategoryJobDTOList(categoryJobList.stream().map(CategoryJob::toDTO).collect(Collectors.toList()));
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
