package ru.iteco.at.center.exception;

import org.jetbrains.annotations.Nullable;

public class InvalidUserTokenException extends RuntimeException {
    public InvalidUserTokenException(@Nullable String userToken) {
        super("Invalid token:" + userToken);
    }
}
