package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.entity.Metrik;

import java.util.List;

public interface MetrikService {

    Metrik save(@NotNull final Metrik metrik);

    List<Metrik> saveAll(@NotNull final Iterable<Metrik> iterable);

    Metrik findById(@NotNull final String id);

    Iterable<Metrik> findAllByJobInstanceId(@NotNull final String jobInstanceId);

    boolean existsById(@NotNull final String id);

    void delete(@NotNull final Metrik metrik);

    void deleteById(@NotNull final String id);

    void deleteAll(Iterable<Metrik> entities);

    void deleteAllByJobInstanceId(@NotNull final String jobInstanceId);

    boolean existsByJobInstanceId(@NotNull String jobInstanceId);
}
