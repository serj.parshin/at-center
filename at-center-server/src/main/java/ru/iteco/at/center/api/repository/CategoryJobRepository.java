package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.CategoryJob;

@Repository
public interface CategoryJobRepository extends JpaRepository<CategoryJob, String> {

    Iterable<CategoryJob> findByJob_id(@NotNull String jobId);

    Iterable<CategoryJob> findByCategory_id(@NotNull String categoryId);

    boolean existsByCategory_Id(@NotNull String id);
}
