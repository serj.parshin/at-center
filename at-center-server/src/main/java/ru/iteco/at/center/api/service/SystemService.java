package ru.iteco.at.center.api.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.model.entity.OperationSystem;

@Service
@Transactional
public interface SystemService {

    OperationSystem save(OperationSystem operationSystem);

    OperationSystem findById(String id);

    boolean existsById(String id);

    void deleteById(String id);

    void deleteAll();

    Iterable<OperationSystem> findAll();

    Iterable<OperationSystem> findAllByJobInstanceId(String jobInstanceId);
}
