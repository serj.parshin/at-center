package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.model.dto.MetrikValueDTO;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "app_metrikvalue")
@EqualsAndHashCode(of = "id", callSuper = true)
public class MetrikValue extends AbstractEntity implements Serializable {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "metrik_id")
    private Metrik metrik;

    @Nullable
    private String value;

    @Nullable
    private Date timeStamp;

    @Nullable
    public MetrikValueDTO toDTO() {
        try {
            final MetrikValueDTO dto = new MetrikValueDTO();
            dto.setId(getId());
            dto.setMetrikId(metrik.getId());
            dto.setValue(value);
            dto.setTimeStamp(timeStamp);
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
