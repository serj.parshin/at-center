package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.MetrikService;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.model.entity.Metrik;
import ru.iteco.at.center.service.JobInstanceServiceImpl;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "jobInstanceBean")
public class JobInstanceManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private JobInstanceServiceImpl jobInstanceService;

    @Autowired
    private MetrikService metrikService;

    private JobInstance jobInstance = new JobInstance();

    public String viewJobInstance(@NotNull final JobInstance jobInstance) {
        this.jobInstance = jobInstance;
        return "pretty:viewJobInstance";
    }

    public Iterable<JobInstance> list(@NotNull final String jobId) {
        return jobInstanceService.findAllByJobId(jobId);
    }

    public boolean metrikExists() {
        return metrikService.existsByJobInstanceId(jobInstance.getId());
    }

    public Iterable<Metrik> metrikList() {
        return metrikService.findAllByJobInstanceId(jobInstance.getId());
    }

    public void deleteJobInstance(final JobInstance jobInstance) {
        jobInstanceService.delete(jobInstance);
    }
}
