package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.entity.MetrikValue;

public interface MetrikValueService {

    MetrikValue save(MetrikValue metrikValue);

    MetrikValue findById(String id);

    boolean existsById(String id);

    void deleteById(String id);

    void delete(MetrikValue metrikValue);

    Iterable<MetrikValue> findAllByMetrik_id(@NotNull String metrikId);

    public boolean existsByMetrik_id(@NotNull String metrikId);

    void deleteAllByMetrik_id(@NotNull String metrikId);
}
