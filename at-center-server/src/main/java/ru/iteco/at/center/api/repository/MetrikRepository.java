package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.Metrik;

@Repository
public interface MetrikRepository extends JpaRepository<Metrik, String> {

    @Nullable
    Iterable<Metrik> findAllByJobInstance_id(@NotNull final String jobInstanceId);

    void deleteAllByJobInstance_id(@NotNull final String jobInstanceId);

    boolean existsByJobInstance_id(@NotNull final String jobInstanceId);
}
