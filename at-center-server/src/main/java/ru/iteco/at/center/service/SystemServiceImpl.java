package ru.iteco.at.center.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.SystemRepository;
import ru.iteco.at.center.api.service.SystemService;
import ru.iteco.at.center.exception.SystemNotFoundException;
import ru.iteco.at.center.model.entity.OperationSystem;

@Service
@Transactional
public class SystemServiceImpl implements SystemService {

    @NotNull
    final SystemRepository systemRepository;

    @Autowired
    public SystemServiceImpl(@Nullable SystemRepository systemRepository) {
        this.systemRepository = systemRepository;
    }

    public OperationSystem save(OperationSystem operationSystem) {
      return systemRepository.save(operationSystem);
    }

    public OperationSystem findById(String id) {
        return systemRepository.findById(id).orElseThrow(SystemNotFoundException::new);
    }

    @Override
    public boolean existsById(String id) {
        return systemRepository.existsById(id);
    }

    public void deleteById(String id) {
        systemRepository.deleteById(id);
    }

    public void deleteAll() {
        systemRepository.deleteAll();
    }

    @Override
    public Iterable<OperationSystem> findAll() {
        systemRepository.findAll();
        return null;
    }

    @Override
    public Iterable<OperationSystem> findAllByJobInstanceId(String id) {
        return systemRepository.findAll();
    }
}
