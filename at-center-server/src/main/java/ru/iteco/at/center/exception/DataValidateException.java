package ru.iteco.at.center.exception;

import org.jetbrains.annotations.NotNull;

public class DataValidateException extends RuntimeException {

public DataValidateException(
@NotNull final String message) {
        super("#" + message);
        }
        }
