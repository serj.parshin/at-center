package ru.iteco.at.center.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.iteco.at.center.api.rest.JobController;
import ru.iteco.at.center.api.service.*;
import ru.iteco.at.center.exception.InvalidUserTokenException;
import ru.iteco.at.center.exception.JobDTOIsNullException;
import ru.iteco.at.center.model.dto.*;
import ru.iteco.at.center.model.entity.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/job")
public class JobControllerImpl implements JobController {

    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobInstanceService jobInstanceService;

    @Autowired
    private SystemService systemService;

    @Autowired
    private MetrikService metrikService;

    @Autowired
    private MetrikValueService metrikValueService;

    @Autowired
    private TokenService tokenService;

    @Override
    @Transactional
    @PostMapping(consumes = "application/json")
    public void post(@RequestBody JobDTO jobDTO) {
        try {
            if (jobDTO == null) throw new JobDTOIsNullException();
            if (!tokenService.validateToken(jobDTO.getUserToken()))
                throw new InvalidUserTokenException(jobDTO.getUserToken());
            final Job job = parseJobFromDTO(jobDTO);
            jobService.save(job);
        } catch (JobDTOIsNullException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        } catch (InvalidUserTokenException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage(), ex);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
        }
    }

    private Job parseJobFromDTO(@NotNull final JobDTO jobDTO) {
        final Job job;
        final String jobId = jobDTO.getId();
        final JobInstanceDTO jobInstanceDTO = jobDTO.getJobInstanceDTO();
        jobInstanceDTO.setUserId(userService.findIdByToken(jobDTO.getUserToken()));
        if (jobService.existsById(jobId)) job = jobService.findById(jobId);
        else {
            final Job newJob = new Job();
            newJob.setId(jobDTO.getId());
            newJob.setName(jobDTO.getName());
            job = jobService.save(newJob);
        }
        job.setBuildNumber(jobDTO.getBuildNumber());
        job.getJobInstanceList().add(parseJobInstanceFromDTO(jobInstanceDTO, job));
        return job;
    }

    private JobInstance parseJobInstanceFromDTO(@NotNull final JobInstanceDTO jobInstanceDTO,
                                                @NotNull final Job job) {
        final JobInstance jobInstance;
        if (jobInstanceService.existsById(jobInstanceDTO.getId())) jobInstance =
                jobInstanceService.findById(jobInstanceDTO.getId());
        else {
            final JobInstance newJobInstance = new JobInstance();
            newJobInstance.setId(jobInstanceDTO.getId());
            newJobInstance.setJob(job);
            newJobInstance.setUser(userService.findById(jobInstanceDTO.getUserId()));
            jobInstance = jobInstanceService.save(newJobInstance);
        }
        jobInstance.setDateBegin(jobInstanceDTO.getDateBegin());
        jobInstance.setOperationSystem(parseSystemFromDTO(jobInstanceDTO.getSystemDTO()));
        jobInstance.setStatusJob(jobInstanceDTO.getStatusJob());
        jobInstance.setDateEnd(jobInstanceDTO.getDateEnd());
        jobInstance.getMetrikList().addAll(parseMetrikListFromDTO(jobInstanceDTO.getMetrikDTOList(), jobInstance));
        return jobInstance;
    }

    private Collection<? extends Metrik> parseMetrikListFromDTO(@Nullable List<MetrikDTO> metrikDTOList, JobInstance jobInstance) {
        if (metrikDTOList == null) return new ArrayList<>();
        final List<Metrik> metriks = new ArrayList<>();
        metrikDTOList.forEach(metrikDTO -> {
            metriks.add(parseMetrikFromDTO(metrikDTO, jobInstance));
        });
        return metriks;
    }

    private Metrik parseMetrikFromDTO(@Nullable final MetrikDTO metrikDTO, JobInstance jobInstance) {
        if (metrikDTO == null || jobInstance == null) return null;
        final Metrik metrik;
        if (metrikService.existsById(metrikDTO.getId())) metrik = metrikService.findById(metrikDTO.getId());
        else {
            final Metrik newMetrik = new Metrik();
            newMetrik.setJobInstance(jobInstance);
            newMetrik.setName(metrikDTO.getName());
            newMetrik.setType(metrikDTO.getType());
            metrik = metrikService.save(newMetrik);
        }
        metrik.getValues().addAll(parseMetrikValueListFromDTO(metrikDTO.getMetrikValueDTOList(), metrik));
        return metrik;
    }

    private Collection<? extends MetrikValue> parseMetrikValueListFromDTO(@Nullable final List<MetrikValueDTO> metrikValueDTOList, Metrik metrik) {
        if (metrikValueDTOList == null) return new ArrayList<>();
        final List<MetrikValue> metrikValues = new ArrayList<>();
        metrikValueDTOList.forEach(metrikValueDTO -> {
            metrikValues.add(parseMetrikValueFromDTO(metrikValueDTO, metrik));
        });
        return metrikValues;
    }

    private MetrikValue parseMetrikValueFromDTO(@Nullable final MetrikValueDTO metrikValueDTO, Metrik metrik) {
        if (metrikValueDTO == null) return null;
        final MetrikValue metrikValue;
        if (metrikService.existsById(metrikValueDTO.getId()))
            metrikValue = metrikValueService.findById(metrikValueDTO.getId());
        else {
            final MetrikValue newMetrikValue = new MetrikValue();
            newMetrikValue.setId(metrikValueDTO.getId());
            newMetrikValue.setValue(metrikValueDTO.getValue());
            newMetrikValue.setTimeStamp(metrikValueDTO.getTimeStamp());
            newMetrikValue.setMetrik(metrik);
            metrikValue = metrikValueService.save(newMetrikValue);
        }
        return metrikValue;
    }

    private OperationSystem parseSystemFromDTO(@Nullable final SystemDTO systemDTO) {
        if (systemDTO == null) return null;
        final OperationSystem operationSystem;
        if (systemService.existsById(systemDTO.getId())) operationSystem = systemService.findById(systemDTO.getId());
        else {
            OperationSystem newOperationSystem = new OperationSystem();
            newOperationSystem.setId(systemDTO.getId());
            newOperationSystem.setName(systemDTO.getName());
            newOperationSystem.setSystemCapacity(systemDTO.getSystemCapacity());
            operationSystem = systemService.save(newOperationSystem);
        }
        return operationSystem;
    }
}
