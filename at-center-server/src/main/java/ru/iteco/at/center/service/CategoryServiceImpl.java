package ru.iteco.at.center.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.api.repository.CategoryRepository;
import ru.iteco.at.center.api.service.CategoryService;
import ru.iteco.at.center.model.entity.Category;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional
    public Category save(Category cate) {
        return categoryRepository.save(cate);
    }

    public Optional<Category> findById(String id) {
        return categoryRepository.findById(id);
    }

    public boolean existsById(String id) {
        return categoryRepository.existsById(id);
    }

    @Transactional
    public void deleteById(String id) {
        categoryRepository.deleteById(id);
    }

    @Transactional
    public void delete(Category category) {
        categoryRepository.delete(category);
    }

    @Transactional
    public void deleteAll(Iterable<? extends Category> categories) {
        categoryRepository.deleteAll(categories);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }
}
