package ru.iteco.at.center.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(@NotNull final String login);

    boolean existsByToken(@NotNull final String token);

    boolean existsByLogin(@NotNull final String login);

    Optional<User> findOptionalByToken(@NotNull final String token);

    @Query("SELECT u.id FROM User u WHERE u.token = ?1")
    String findIdByToken(String token);
}
