package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.at.center.model.entity.JobInstance;

public interface JobInstanceService {

    Iterable<JobInstance> findAllByJobId(@NotNull final String jobId);

    JobInstance save(@NotNull final JobInstance job);

    JobInstance findById(@NotNull final String id);

    JobInstance findByIdAndUserId(@NotNull final String id, @NotNull final String jobId);

    boolean existsById(@NotNull final String id);

    boolean existsByJobId(@NotNull final String jobId);

    void deleteById(@NotNull final String id);

    void delete(@NotNull final JobInstance jobInstance);
}
