package ru.iteco.at.center.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.iteco.at.center.enumerate.Role;
import ru.iteco.at.center.model.dto.UserDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "app_user")
@EqualsAndHashCode(of = "id", callSuper = true)
public class User extends AbstractEntity implements UserDetails, Serializable {

    @Nullable
    @Column(unique = true)
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String token;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Nullable
    private String email = "admin";

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<JobInstance> jobInstance = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorityList = new java.util.ArrayList<>();
        authorityList.add(new SimpleGrantedAuthority(role.name()));
        return authorityList;
    }

    @Nullable
    @Override
    public String getPassword() {
        return password;
    }

    @Nullable
    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Nullable
    public UserDTO toDTO() {
        try {
            final UserDTO dto = new UserDTO();
            dto.setId(getId());
            dto.setLogin(getLogin());
            dto.setPassword(getPassword());
            dto.setRole(getRole());
            dto.setToken(getToken());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
