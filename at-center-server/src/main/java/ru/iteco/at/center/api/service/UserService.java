package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.at.center.model.entity.User;

@Service
@Transactional
public interface UserService {

    @NotNull
    User findLoggedUser();

    @NotNull
    User findById(String id);

    User create(User user);

    User generateToken(User user);

    User findByToken(String token);

    String findIdByToken(String token);
}
