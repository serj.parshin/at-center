package ru.iteco.at.center.exception;

public class JobDTOIsNullException extends RuntimeException {
    public JobDTOIsNullException() {
        super("JobDTO is null.");
    }
}
