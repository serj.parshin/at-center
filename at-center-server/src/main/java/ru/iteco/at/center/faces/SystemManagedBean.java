package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.SystemService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.model.entity.OperationSystem;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "systemBean")
public class SystemManagedBean extends SpringBeanAutowiringSupport {

    @Nullable
    @Autowired
    private SystemService systemService;

    @Autowired
    @Nullable
    private UserService userService;

    @Nullable
    private OperationSystem operationSystem = new OperationSystem();

    @Nullable
    private List<System> filteredSystem;

    public List<System> getFilteredSystem() {
        return filteredSystem;
    }

    public void setFilteredSystem(List<System> filteredSystem) {
        this.filteredSystem = filteredSystem;
    }

    public String createSystem() {
        operationSystem = new OperationSystem();
        return "pretty:createSystem";
    }

    public String editSystem(@NotNull final OperationSystem operationSystem) {
        this.operationSystem = operationSystem;
        return "pretty:editSystem";
    }

    public String saveSystem() {
        systemService.save(operationSystem);
        return "pretty:systemList";
    }

    public void deleteSystem(@Nullable OperationSystem operationSystem) {
        systemService.deleteById(operationSystem.getId());
    }

    public Iterable<OperationSystem> list() {
        return systemService.findAll();
    }
}
