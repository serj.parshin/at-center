package ru.iteco.at.center.api.service;

import ru.iteco.at.center.model.entity.Category;

import java.util.Optional;

public interface CategoryService {

    Category save(Category cate);

    Optional<Category> findById(String id);

    boolean existsById(String id);

    void deleteById(String id);

    void delete(Category category);

    void deleteAll(Iterable<? extends Category> categories);

    Iterable<Category> findAll();
}
