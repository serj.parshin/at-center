package ru.iteco.at.center.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.iteco.at.center.api.service.MetrikService;
import ru.iteco.at.center.api.service.MetrikValueService;
import ru.iteco.at.center.api.service.UserService;
import ru.iteco.at.center.enumerate.TypeMetrik;
import ru.iteco.at.center.model.entity.JobInstance;
import ru.iteco.at.center.model.entity.Metrik;
import ru.iteco.at.center.model.entity.MetrikValue;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@Setter
@SessionScoped
@ManagedBean(name = "metrikBean")
public class MetrikManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private MetrikService metrikService;

    @Autowired
    private MetrikValueService metrikValueService;

    @Autowired
    private UserService userService;

    private Metrik metrik = new Metrik();

    public String createMetrik(@NotNull final JobInstance jobInstance) {
        metrik = new Metrik(jobInstance);
        return "pretty:createMetrik";
    }

    public String editMetrik(@NotNull final Metrik metrik) {
        this.metrik = metrik;
        return "pretty:editMetrik";
    }

    public String viewMetrik(@NotNull final Metrik metrik) {
        this.metrik = metrik;
        return "pretty:viewMetrik";
    }

    public String saveMetrik() {
        metrikService.save(metrik);
        return "pretty:viewJob";
    }

    public Iterable<Metrik> list(@NotNull final String jobId) {
        return metrikService.findAllByJobInstanceId(jobId);
    }

    public Iterable<MetrikValue> metrikValues() {
        return metrikValueService.findAllByMetrik_id(metrik.getId());
    }

    public boolean valuesExists() {
        return metrikValueService.existsByMetrik_id(metrik.getId());
    }

    public TypeMetrik[] getMetrikTypes() {
        return TypeMetrik.values();
    }

    public void deleteMetrik(@NotNull final Metrik metrik) {
        metrikService.delete(metrik);
    }

}
