package ru.iteco.at.center.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.at.center.model.entity.OperationSystem;

@Repository
public interface SystemRepository extends JpaRepository<OperationSystem, String> {
}
