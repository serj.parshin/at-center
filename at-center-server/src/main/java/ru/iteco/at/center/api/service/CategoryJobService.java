package ru.iteco.at.center.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.at.center.model.entity.Category;
import ru.iteco.at.center.model.entity.Job;

public interface CategoryJobService {

    @Nullable
    Iterable<Category> findAllCategoriesByJobId(@NotNull final String jobId);

    @Nullable
    Iterable<Job> findAllJobByCategoryId(@NotNull final String categoryId);

    boolean existsByCategoryId(@NotNull final String id);
}
